#!/usr/bin/env bash

set -e

# Parameters (defaults).
sudo_rights=false

# Parameter parsing.
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
      --sudo)
      sudo_rights=true
      sudo whoami
      shift
      ;;
      *)
      shift
      ;;
  esac
done

# If not installed, install now.
if [[ -z "$P_INSTALLED" ]]; then
  if $sudo_rights; then
    current_user=$(whoami)
    sudo apt install git zsh fonts-powerline wget curl -y
    sudo usermod --shell /usr/bin/zsh $current_user
  fi
  mkdir -p "$HOME/code/scripts" || true 2> /dev/null
  pushd "$HOME/code/scripts" > /dev/null
  if [[ ! -d "setup_env" ]]; then
    git clone git@gitlab.com:dreher/setup_env.git
    cp "setup_env/env_conf.tmpl.sh" "setup_env/env_conf.sh"
  fi
  if [[ ! -d "p" ]]; then
    git clone git@gitlab.com:dreher/p.git
  fi
  mkdir bin || true 2> /dev/null
  pushd bin > /dev/null
  ln -s ../setup_env/setup_env.sh setup_env || true 2> /dev/null
  popd > /dev/null
  popd > /dev/null

  printf "\n\n"
  echo "ATTENTION! Now open a new terminal and follow these instructions."
  echo
  echo "Add these lines to your .bashrc or .zshrc:"
  echo "export P_CONFIG_DIR=\"\$HOME/code/scripts/setup_env/p_templates\""
  echo "export PATH=\"\$HOME/.local/bin:\$PATH\""
  echo "export PATH=\"\$HOME/code/scripts/bin:\$PATH\""
  echo "source \$HOME/code/scripts/p/shell/p.sh"
  echo
  echo "Configure the environment via the setup_env configuration file:"
  echo "vim \$HOME/code/scripts/setup_env/env_conf.sh"
  echo
  echo "To install oh-my-zsh, run:"
  echo "sh -c \"\$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)\""
  echo
  echo "After you have done this, close this terminal, open a new one, and run 'setup_env'."
  echo "You will need to log out and back in so that zsh is the default shell."
fi
